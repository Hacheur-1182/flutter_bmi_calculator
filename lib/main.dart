import 'package:bmi/helper/color_app.dart';
import 'package:bmi/pages/home/home_page.dart';
import 'package:bmi/provider/result_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'helper/Custom_transition_page.dart';

void main() {
  runApp(
    const BmiCalculator(),
  );
}

class BmiCalculator extends StatelessWidget {
  const BmiCalculator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => BmiResult(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData.dark().copyWith(
          primaryColor: kPrimaryColor,
          scaffoldBackgroundColor: kPrimaryColor,
          appBarTheme: const AppBarTheme(backgroundColor: kPrimaryColor),
          pageTransitionsTheme: PageTransitionsTheme(builders: {
            TargetPlatform.android: CustomPageTransitionBuilder(),
            TargetPlatform.iOS: CustomPageTransitionBuilder(),
          })
        ),
        home: const Home(),
      ),
    );
  }
}
