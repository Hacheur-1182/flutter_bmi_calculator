import 'package:bmi/components/card_gender.dart';
import 'package:bmi/components/card_parameter.dart';
import 'package:bmi/helper/calculate_bmi.dart';
import 'package:bmi/helper/color_app.dart';
import 'package:bmi/pages/result/result_page.dart';
import 'package:flutter/material.dart';

import '../drawer/drawer-page.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

enum Gender { male, female }

Gender _gender = Gender.female;
int _weight = 52;
int _age = 22;
int _size = 170;
bool _isLoading = false;

class _HomeState extends State<Home> {
  final key = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: key,
        appBar: AppBar(
          title: const Text("BMI CALCULATOR"),
          centerTitle: true,
          elevation: 0.0,
          leading: IconButton(
            icon: const Icon(
              Icons.sort,
              size: 40,
            ),
            onPressed: () {
              key.currentState!.openDrawer();
            },
          ),
        ),
        drawer: const DrawerPage(),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          _gender = Gender.male;
                        });
                      },
                      child: CardGender(
                        text: "MALE",
                        iconGender: Icons.male,
                        cardGenderColor: _gender == Gender.male
                            ? kCardColor
                            : kDisableCardColor,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 10.0,
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          _gender = Gender.female;
                        });
                      },
                      child: CardGender(
                        text: 'FEMALE',
                        iconGender: Icons.female,
                        cardGenderColor: _gender == Gender.female
                            ? kCardColor
                            : kDisableCardColor,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 200,
              margin: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                color: kCardColor,
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RichText(
                    text: TextSpan(children: [
                      TextSpan(
                        text: "$_size",
                        style: const TextStyle(
                          fontSize: 40.0,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                      const TextSpan(
                        text: "cm",
                        style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                    ]),
                  ),
                  SliderTheme(
                    data: SliderTheme.of(context).copyWith(
                      inactiveTrackColor: const Color(0xFF8D8E98),
                      activeTrackColor: Colors.white,
                      thumbColor: const Color(0xFFEB1555),
                      overlayColor: const Color(0x29EB1555),
                      thumbShape: const RoundSliderThumbShape(
                        enabledThumbRadius: 15.0,
                      ),
                      overlayShape: const RoundSliderOverlayShape(
                        overlayRadius: 30.0,
                      ),
                    ),
                    child: Slider(
                      label: "cm",
                      value: _size.toDouble(),
                      min: 110,
                      max: 200,
                      onChanged: (double newValue) {
                        setState(() {
                          _size = newValue.round();
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    child: CardParameter(
                      text: "WEIGHT",
                      counter: _weight,
                      pressedMax: () {
                        setState(() {
                          _weight++;
                        });
                      },
                      pressedMin: () {
                        setState(() {
                          _weight--;
                        });
                      },
                    ),
                  ),
                  const SizedBox(
                    width: 10.0,
                  ),
                  CardParameter(
                    text: 'AGE',
                    counter: _age,
                    pressedMax: () {
                      setState(() {
                        _age++;
                      });
                    },
                    pressedMin: () {
                      setState(() {
                        _age--;
                      });
                    },
                  ),
                ],
              ),
            ),
            TextButton(
              onPressed: () {
                setState(() {
                  _isLoading = true;
                });
                BmiCalculator calculate =
                    BmiCalculator(height: _size, weight: _weight);

                Future.delayed(
                    const Duration(seconds: 1),
                    () => {
                          setState(() {
                            _isLoading = false;
                          }),
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ResultPage(
                                titleResult: calculate.resultTitle(),
                                text: calculate.interpretation(),
                                result: calculate.calculateBmi(),
                              ),
                            ),
                          )
                        });
              },
              child: _isLoading
                  ? const CircularProgressIndicator(
                      color: kPrimaryColor,
                    )
                  : const Text(
                      "CALCULATE",
                      style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                      ),
                    ),
              style: TextButton.styleFrom(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                backgroundColor: const Color(0xFFEB1555),
              ),
            )
          ],
        ),
      ),
    );
  }
}
