import 'package:bmi/helper/color_app.dart';
import 'package:bmi/pages/home/home_page.dart';
import 'package:bmi/provider/result_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ResultPage extends StatelessWidget {
  const ResultPage({
    Key? key,
    required this.titleResult,
    required this.text,
    required this.result,
  }) : super(key: key);

  final String titleResult;
  final String text;
  final double result;

  @override
  Widget build(BuildContext context) {
    final saveResult = Provider.of<BmiResult>(context);
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("BMI CALCULATOR"),
          centerTitle: true,
          elevation: 2.0,
          shadowColor: const Color(0xFF002171),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              CupertinoIcons.chevron_back,
            ),
          ),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const Padding(
              padding: EdgeInsets.only(
                top: 18.0,
                left: 20.0,
                right: 20.0,
                bottom: 18.0,
              ),
              child: Text(
                "Your Result",
                style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 45.0,
                ),
              ),
            ),
            Expanded(
              child: Container(
                height: 400,
                width: 700,
                margin: const EdgeInsets.all(20.0),
                decoration: BoxDecoration(
                  color: kDisableCardColor,
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 60.0,
                    ),
                    Text(
                      titleResult,
                      style: const TextStyle(
                        color: Color(0xFF24D876),
                        fontWeight: FontWeight.w900,
                        fontSize: 20.0,
                      ),
                    ),
                    const SizedBox(
                      height: 60.0,
                    ),
                    Text(
                      result.toStringAsFixed(1),
                      style: const TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: 80.0,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 78.0,
                        left: 20.0,
                        right: 20.0,
                        bottom: 40.0,
                      ),
                      child: Text(
                        text,
                        softWrap: true,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    TextButton(
                      style: TextButton.styleFrom(
                        backgroundColor: kPrimaryColor,
                        padding: const EdgeInsets.symmetric(
                          horizontal: 50,
                          vertical: 20.0,
                        ),
                      ),
                      onPressed: () {
                        saveResult.addResult(
                          titleResult,
                          result.toStringAsFixed(1),
                          DateTime.now()
                        );
                      },
                      child: const Text(
                        "SAVE RESULT",
                        style: TextStyle(
                          color: CupertinoColors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Home(),
                  ),
                  (route) => false,
                );
              },
              child: const Text(
                "RE-CALCULATE",
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                ),
              ),
              style: TextButton.styleFrom(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                backgroundColor: const Color(0xFFEB1555),
              ),
            )
          ],
        ),
      ),
    );
  }
}
