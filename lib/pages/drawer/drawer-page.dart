import 'package:bmi/helper/color_app.dart';
import 'package:bmi/provider/result_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DrawerPage extends StatefulWidget {
  const DrawerPage({Key? key}) : super(key: key);

  @override
  State<DrawerPage> createState() => _DrawerPageState();
}

class _DrawerPageState extends State<DrawerPage> {

  @override
  Widget build(BuildContext context) {
    final results = Provider.of<BmiResult>(context).results;
    return Drawer(
      backgroundColor: kPrimaryColor,
      child: CustomScrollView(
        slivers: [
          const SliverAppBar(
            title: Text("My Results"),
            automaticallyImplyLeading: false,
            centerTitle: true,
            pinned: true,
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index)
              {
                return  ListTile(
                  tileColor: kDisableCardColor,
                  title: Text(
                    results[index].score,
                    style: const TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 18.0,
                    ),
                  ),
                  subtitle: Text(results[index].title),
                  trailing: Text(results[index].date.toIso8601String().split("T")[0]),
                );
              },
              childCount: results.length,
            ),
          ),
        ],
      ),
    );
  }
}
