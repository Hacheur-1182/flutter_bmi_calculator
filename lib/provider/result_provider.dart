import 'dart:collection';

import 'package:flutter/cupertino.dart';

import '../models/result_model.dart';

class BmiResult with ChangeNotifier {
  final List<ResultModel> _result = [];

  UnmodifiableListView<ResultModel> get results {
    return UnmodifiableListView(_result);
  }

  void addResult(String title, String score, DateTime date) {
    _result.add(ResultModel(title: title, score: score, date: date));

    notifyListeners();
  }
}
