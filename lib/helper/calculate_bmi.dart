import 'dart:math';

class BmiCalculator {
  final int height;
  final int weight;
  BmiCalculator({
    required this.height,
    required this.weight,
  });

  double calculateBmi() {
    return (weight / pow(height / 100, 2));
  }

  String resultTitle() {
    if (calculateBmi() >= 25) {
      return "OVERWEIGHT";
    } else if (calculateBmi() > 18.5) {
      return "NORMAL";
    }
    return "UNDERWEIGHT";
  }

  String interpretation() {
    if (calculateBmi() >= 25) {
      return "You have hogher than normal body weight. Try to exercise more";
    } else if (calculateBmi() > 18.5) {
      return "You have a normal body weight. Good job!";
    }
    return "You have a lower than normal body weight. You can eat a bit more.";
  }
}
