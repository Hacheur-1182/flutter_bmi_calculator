import 'dart:ui';

const Color kPrimaryColor = Color(0xFF0A0E21);
const Color kCardColor = Color(0xFF1D1E33);
const Color kDisableCardColor = Color(0xFF111328);