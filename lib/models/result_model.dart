class ResultModel {
  final String title;
  final String score;
  final DateTime date;

  const ResultModel({
    required this.title,
    required this.score,
    required this.date
  });
}
