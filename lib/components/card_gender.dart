import 'package:flutter/cupertino.dart';

class CardGender extends StatelessWidget {
  const CardGender({
    Key? key,
    required this.text,
    required this.iconGender,
    required this.cardGenderColor,
  }) : super(key: key);

  final String text;
  final IconData iconGender;
  final Color cardGenderColor;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      width: 170,
      margin: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: cardGenderColor,
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 30.0),
        child: Column(
          children: [
            Transform.rotate(
              angle: text == "FEMALE" ? 7 : 0,
              child: Icon(
                iconGender,
                size: 95.0,
              ),
            ),
            const Spacer(),
            Text(
              text,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 20.0,
                color: CupertinoColors.systemGrey,
              ),
            ),
            const SizedBox(
              height: 25.0,
            ),
          ],
        ),
      ),
    );
  }
}
