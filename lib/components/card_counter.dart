import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CardCounter extends StatelessWidget {
  const CardCounter({
    Key? key,
    required this.iconCard,
    required this.press,
  }) : super(key: key);

  final IconData iconCard;
  final void Function()? press;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: 50,
      decoration: BoxDecoration(
        color: const Color(0xFF4C4F5E),
        borderRadius: BorderRadius.circular(30),
      ),
      child: IconButton(
        onPressed: press,
        icon: Icon(
          iconCard,
          color: CupertinoColors.white,
        ),
      ),
    );
  }
}
