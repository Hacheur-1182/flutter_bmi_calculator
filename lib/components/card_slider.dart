import 'package:bmi/helper/color_app.dart';
import 'package:flutter/material.dart';

class CardSlider extends StatefulWidget {
  const CardSlider({Key? key}) : super(key: key);

  @override
  State<CardSlider> createState() => _CardSliderState();
}

int _size = 170;

class _CardSliderState extends State<CardSlider> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      decoration: BoxDecoration(
        color: kCardColor,
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RichText(
            text: TextSpan(children: [
              TextSpan(
                text: "$_size",
                style: const TextStyle(
                  fontSize: 40.0,
                  fontWeight: FontWeight.w900,
                ),
              ),
              const TextSpan(
                text: "cm",
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ]),
          ),
          Slider(
            //TODO: SliderTheme
            label: "cm",
            value: _size.toDouble(),
            min: 110,
            max: 200,
            activeColor: const Color(0xFFEB1555),
            inactiveColor: const Color(0xFF8D8E98),
            onChanged: (double newValue) {
              setState(() {
                _size = newValue.round();
              });
            },
          ),
        ],
      ),
    );
  }
}
