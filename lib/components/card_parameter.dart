import 'package:bmi/components/card_counter.dart';
import 'package:flutter/cupertino.dart';

import '../helper/color_app.dart';

class CardParameter extends StatelessWidget {
  const CardParameter({
    Key? key,
    required this.text,
    required this.counter,
    required this.pressedMin,
    required this.pressedMax,
  }) : super(key: key);

  final String text;
  final int counter;
  final Function() pressedMin;
  final Function() pressedMax;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      width: 170,
      margin: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: kCardColor,
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Padding(
        padding: const EdgeInsets.only(
          top: 20.0,
        ),
        child: Column(
          children: [
            Text(
              text,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 18.0,
                color: CupertinoColors.systemGrey,
              ),
            ),
            const SizedBox(
              height: 0.0,
            ),
            Text(
              "$counter",
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 50.0,
                fontWeight: FontWeight.w900,
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CardCounter(
                  iconCard: CupertinoIcons.minus,
                  press: pressedMin,
                ),
                const SizedBox(
                  width: 20,
                ),
                CardCounter(
                  iconCard: CupertinoIcons.add,
                  press: pressedMax,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
